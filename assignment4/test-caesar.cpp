#include "CaesarCipher.hpp"
#include "ioutils.hpp"

#include <iostream>
#include <fstream>

int main(int argc, const char *argv[]) {

	IOUtils io;
	io.openStream(argc,argv);
	std::string input, rawInput, encrypted, decrypted;
	rawInput = io.readFromStream();
	input = rawInput.substr(0, rawInput.size() - 2);
	std::cout << "" << std::endl;
	std::cout << "Original text:\n" << std::endl << input;
	std::cout << "\n-----------------------------------------------" << std::endl;


	CaesarCipher ciper(5);
	encrypted = ciper.encrypt(input);
	std::cout << "Encrypted text:\n" << std::endl << encrypted;
	std::cout << "\n-----------------------------------------------" << std::endl;


	decrypted = ciper.decrypt(encrypted);
	std::cout << "Decrypted text:" << std::endl << decrypted;
	std::cout << "\n-----------------------------------------------" << std::endl;

	if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
	else {
		std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
		return 1;   // Make sure to return a non-zero value to indicate failure
	}

	return 0;
}
