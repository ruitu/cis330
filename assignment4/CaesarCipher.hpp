#ifndef CAESAR_CIPHER_HPP_
#define CAESAR_CIPHER_HPP_

#include "Cipher.hpp"
#include <map>

class CaesarCipher: public Cipher {
  public:
	  CaesarCipher(int key);
	  //virtual ~CaesarCipher();
	  virtual std::string encrypt( std::string &text );
  	  virtual std::string decrypt( std::string &text );

  private:
	  map<int, char> letterMap;
	  map<char, int> keyMap;
	  int _key;
	  int _alphabetRange;
	  int _asciiStart;
	  int _asciiEnd;
      string processMessage(std::string &text, bool encrypt);

};

#endif
