#include "CaesarCipher.hpp"

CaesarCipher::CaesarCipher(int key) :  Cipher(), _asciiStart(33), _asciiEnd(122){
	this->_key = key;
	this->_alphabetRange = this->_asciiEnd - this->_asciiStart;
	for (int i = 0; i < (this->_alphabetRange + 1); i++) {
		letterMap[i] = (char) (this->_asciiStart + i);
		keyMap[ letterMap[i] ] = i;
	}

	string specialChars = " \n\t";

	for (int i = 0; i < specialChars.size(); i++) {
		letterMap[++(this->_alphabetRange)] = specialChars[i];
		keyMap[ letterMap[this->_alphabetRange] ] = this->_alphabetRange;
	}
}

std::string
CaesarCipher::encrypt( std::string &text ) { return processMessage(text, true); }

std::string
CaesarCipher::decrypt(std::string &text) { return processMessage(text, false); }


std::string
CaesarCipher::processMessage(std::string &text, bool encrypt) {
	string processedMessage = "";
	int alphabetNum = (this->_alphabetRange + 1);
	unsigned int newCharIndex = 0;
	for(unsigned int i = 0; i < text.length(); i++) {
		unsigned int charIndex = keyMap[text[i]];
		if (encrypt) {
			newCharIndex = (charIndex + this->_key) % alphabetNum;
		} else {
			newCharIndex = (alphabetNum  + charIndex - this->_key) % alphabetNum;
		}
		processedMessage += letterMap[newCharIndex];
	}
	return processedMessage;
}
