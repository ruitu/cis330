#include "DateCipher.hpp"

DateCipher::DateCipher(string date) : Cipher(), _asciiStart(33), _asciiEnd(122){
	this->_date = date;
	this->_alphabetRange = this->_asciiEnd - this->_asciiStart;
	this->_specialChars = " \n\t";
	for (int i = 0; i < this->_alphabetRange + 1; i++) {
		letterMap[i] = (char) (this->_asciiStart + i);
		keyMap[ letterMap[i] ] = i;
	}
}

std::string
DateCipher::encrypt( std::string &text ) { return processMessage(text, true); }

std::string
DateCipher::decrypt(std::string &text) { return processMessage(text, false); }

std::string
DateCipher::getNumberPattern(std::string &text) {
	string numberPattern = "";
	int j = 0;
	for (int i = 0; i < text.size(); i++) {
		char specialChar = 0;
		for (int a = 0; a < (this->_specialChars).size(); a++) {
			if (text[i] == this->_specialChars[a]) {
				specialChar = this->_specialChars[a];
			}
		}

		if (specialChar != 0) {
			numberPattern += specialChar;
		} else {
			numberPattern += this->_date[j % this->_date.size()];
			j++;
		}

	}
	return numberPattern;
}

char
DateCipher::shiftChar(char &aChar, int shiftSpace, bool encrypt) {
	int alphabetNum = this->_alphabetRange + 1;
	unsigned int charIndex = keyMap[aChar];
	unsigned int newCharIndex = 0;
	if (encrypt) {
		newCharIndex = (charIndex + shiftSpace) % alphabetNum;
	} else {
		newCharIndex = (alphabetNum  + charIndex - shiftSpace) % alphabetNum;
	}
	return letterMap[newCharIndex];
}

std::string
DateCipher::processMessage(std::string &text, bool encrypt) {
	string numberPattern = getNumberPattern(text);
	string processedMes = "";

	for (int i = 0; i < numberPattern.size(); i++) {

		char specialChar = 0;

		for (int a = 0; a < (this->_specialChars).size(); a++) {
			if (text[i] == this->_specialChars[a]) {
				specialChar = this->_specialChars[a];
			}
		}

		if (specialChar != 0) {
			processedMes += specialChar;
		} else {
			if (encrypt) {
				processedMes += shiftChar(text[i], (int) (numberPattern[i] - '0'), true);
			} else {
				processedMes += shiftChar(text[i], (int) (numberPattern[i] - '0'), false);
			}
		}
	}
	return processedMes;
}
