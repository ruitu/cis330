#ifndef DATE_CIPHER_HPP_
#define DATE_CIPHER_HPP_

#include "Cipher.hpp"
#include <map>

class DateCipher: public Cipher {
  public:
	  DateCipher(string date);
	  //virtual ~DateCipher();
	  virtual std::string encrypt( std::string &text );
  	  virtual std::string decrypt( std::string &text );

  private:
	  string _date;
      string _specialChars;
	  map<int, char> letterMap;
	  map<char, int> keyMap;

	  int _alphabetRange;
	  int _asciiStart;
	  int _asciiEnd;
      int  convertToInt( char charNum );
	  char shiftChar(char &aChar, int shiftSpace, bool encrypt);
	  char shiftCharBack(char &aChar, int shiftSpace);

	  std::string processMessage(std::string &text, bool encrypt);
	  std::string getNumberPattern(std::string &text);
};


#endif
