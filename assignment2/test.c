#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "triangle.h"

void clearBuffer() {
  while ( getchar() != '\n' );
}

int getInt(const char* message, int lowerBound, int higherBound) {

	int numInts = 0, num = 0;
	while (numInts != 1 || num < lowerBound || num > higherBound) {
	    printf("%s",message);
	    numInts = scanf("%d", &num);
	    clearBuffer();
	}
	return num;
}

int main() {
  int i = 0, numInts=0;
  int **triangle;

  /* Problem 1 */
  printf("Problem 1 (a triangle of height 5):\n");
  print5Triangle();

  /* Problem 2 */
  printf("\nProblem 2:\n");
  const int height = getInt("Please enter the height of the triangle [1-5]: ", 1, 5);
  allocateNumberTriangle(height, &triangle );
  initializeNumberTriangle(height, triangle );
  printNumberTriangle(height, triangle );

  // destructor
  deallocateNumberTriangle(height, triangle );

  return 0;
}
