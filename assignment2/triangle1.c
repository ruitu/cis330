#include <stdio.h>
#include <stdlib.h>
#define ROW 5
#define COL (ROW - 1) * 2 + 1


void print5Triangle() {
	int curRow, curCol, curEmptyEntry;
	int triangle[ROW][COL];
	for (curRow = 0; curRow < ROW; curRow++) {
		for(curCol = 0; curCol < COL; curCol++) {
			triangle[curRow][curCol] = 0;
		}
	}


	for (curRow = 0; curRow < ROW; curRow++) {
		for (curCol = 0; curCol < 2 * (curRow) + 1; curCol++) {
			triangle[curRow][curCol] = curCol;
		}
	}

	for (curRow = 0; curRow < ROW; curRow++) {
		int emptySpaceNum = ROW - 1 - curRow, curEmptyRow;
		for (curEmptyRow = 0; curEmptyRow < emptySpaceNum; curEmptyRow++)
    		printf("  ");
		for (curCol = 0; curCol < 2 * curRow + 1; curCol++)
			printf("%d ", curCol);
		printf("\n");
	}
	printf("\n");
}
