#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void allocateNumberTriangle( const int height, int *** triangle ) {
	int i;
	( *triangle ) = ( int ** ) malloc( height * sizeof( int *) );
	for (i = 0; i < height; i++) {
		( *triangle )[i] = (int *) malloc ( ((height - 1) * 2 + 1) * sizeof(int) );
	  	memset(( *triangle )[i], 0, height * sizeof(int)); // set values to 0
	}
}

void initializeNumberTriangle(const int height, int **triangle) {
	int curRow, curCol, width  = (height - 1) * 2 + 1;
	for (curRow = 0; curRow < height; curRow++) {
		for(curCol = 0; curCol < width; curCol++) {
			triangle[curRow][curCol] = 0;
		}
	}

	for (curRow = 0; curRow < height; curRow++) {
		for (curCol = 0; curCol < 2 * (curRow) + 1; curCol++) {
			triangle[curRow][curCol] = curCol;
		}
	}
}


void printNumberTriangle(const int height, int **triangle) {
	int curRow, curCol, curEmptyEntry;
	for (curRow = 0; curRow < height; curRow++) {
		int emptySpaceNum = height - 1 - curRow, curEmptyRow;
		for (curEmptyRow = 0; curEmptyRow < emptySpaceNum; curEmptyRow++) {
    		printf("  ");
		}

		for (curCol = 0; curCol < 2 * curRow + 1; curCol++) {
			printf("%d ", curCol);
		}
		printf("\n");
	}
}


void deallocateNumberTriangle(const int height, int **triangle) {
	int i, j;
	for (i = 0; i < height; i++) {
		free( (triangle)[i] );
	}
	free( (triangle) );
}


int takeInput(void) {
	int number, success;
	printf("Please enter the height of the triangle [1-5]: \n");

	while (!(success = scanf("%d", &number)) || number < 1 || number > 5) {
		printf("The number you typed was %d\n", number);
		printf("Please re-enter the height of the triangle [1-5]: \n");
	}

	return number;
}
