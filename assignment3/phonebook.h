/*
 * learned linked list from this tutorial
 * http://www.thegeekstuff.com/2012/08/c-linked-list-example/
 *
 */

#include<stdio.h>
#include<stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>



#define TEXTSIZE 50

typedef struct PhoneBook {
	char name[TEXTSIZE];
	char number[TEXTSIZE];
	struct PhoneBook* next;
} Contact;


extern Contact *head;
extern Contact *curr;


Contact* creatPhoneBook(char *contactName, char *contactNumber);
Contact* addToPhoneBook(char *contactName, char *contactNumber, bool addToEnd);
void printPhoneBook(void);
void enterName();
int deleteEntry();
