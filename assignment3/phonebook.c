#include "phonebook.h"

Contact *head = NULL;
Contact *curr = NULL;

Contact* creatPhoneBook(char *contactName, char *contactNumber) {
    Contact *ptr = (Contact*)malloc(sizeof(Contact));
    if(NULL == ptr)
        return NULL;

	strcpy(ptr->name, contactName);
	strcpy(ptr->number, contactNumber);
    ptr->next = NULL;

    head = curr = ptr;
    return ptr;
}

Contact* addToPhoneBook(char *contactName, char *contactNumber, bool addToEnd) {
    if(NULL == head)
        return (creatPhoneBook(contactName, contactNumber));

    Contact *ptr = (Contact*)malloc(sizeof(Contact));
    if(NULL == ptr)
        return NULL;

	strcpy(ptr->name, contactName);
	strcpy(ptr->number, contactNumber);
    ptr->next = NULL;

    if(addToEnd) {
        curr->next = ptr;
        curr = ptr;
    } else {
        ptr->next = head;
        head = ptr;
    }

    return ptr;
}


void printPhoneBook(void){
    Contact *ptr = head;

	if (head == NULL)
		printf("\nNo Entry to be Displayed\n");
	else {
		printf("\nCurrent Directory:\n");
		int counter = 1;
	    while(ptr != NULL) {
	        printf("%d. %s, %s\n", counter, ptr->name, ptr->number);
			counter++;
	        ptr = ptr->next;
	    }
	}
}

void enterName(){
	char newName[TEXTSIZE];
	char newNumber[TEXTSIZE];
	printf("\nEnter a name: ");
	scanf("%s", newName);
	printf("Enter a number: ");
	scanf("%s", newNumber);
	addToPhoneBook(newName, newNumber, true);
}

int deleteEntry() {
	int deleteIndex, i;
	Contact *ptr = head;
	Contact *prev = NULL;
	Contact *del  = NULL;

	printPhoneBook();
	printf("Who would you wipe off: ");
	scanf("%d", &deleteIndex);

	if (deleteIndex == 1) {
		del = head;
	} else {
		for (i = 1; i < deleteIndex - 1; i++)
			ptr = ptr->next;

		prev = ptr;
		del  = prev->next;
	}

	if(del == NULL) {
		return -1;
	} else
		if(prev != NULL)
			prev->next = del->next;
		if(del == curr && del->name != head->name) {
			curr = prev;
		} else if(del->name == head->name && del->next != NULL) {
			head = del->next;
		} else if (del->name == head->name && del->next == NULL){
			head = NULL;
			curr = NULL;
		}

	free(del);
	del = NULL;
	return 0;
}



int main(void) {
	while (true) {
		printf("Please choose an option:\n1. Insert a new entry.\n2. Delete an entry.\n3. Display current directory.\n");
		int userInput;
		printf("option: ");
		scanf("%d", &userInput);
		switch (userInput) {
			case(1):
				enterName();
				break;
			case(2):
				deleteEntry();
				break;
			case(3):
				printPhoneBook();
				break;
		}
		printf("\n");
	}
    return 0;
}
