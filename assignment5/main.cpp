#include "Game.hpp"

int main(){

    const string sizeMes  = "Please enter the size of the grid (int int): ";
    const string steosMes = "Please enter the number of steps (int): ";

    int height  = 0;
    int weighth = 0;
    std::cout << sizeMes << std::endl;
    cin >> height;
    cin >> weighth;

    int steps  = 0;
    std::cout << steosMes << std::endl;
    cin >> steps;

    Game game(height, weighth, steps);


    std::cout << "=========GAME BEGIN==========" << std::endl;
    game.printMap();
    std::cout << "" << std::endl;
    for (int i = 0; i < steps; i++ ){
        std::cout << "Step " << i + 1 << std::endl;
        game.nextStep();
        game.printMap();
        std::cout << "" << std::endl;
    }

}
