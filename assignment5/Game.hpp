#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>

#ifndef GAME_H
#define GAME_H

using namespace std;

class Game{
public:
    Game(int height, int weight, int steps);
	~Game() { } ;
    void printMap();
    void nextStep();


private:
	int _height;
    int _weight;
    int _steps;
    vector< vector<string> > _world;
    vector<string*> getNeighbors(int i, int j);
    bool overpopulation(string gameFigure, vector<string*> neighbors);
    bool predation(string gameFigure, vector<string*> neighbors);
    bool wolfKilledByFammer(string gameFigure, vector<string*> neighbors);
    bool starvation(string gameFigure, vector<string*> neighbors);
    void emigration(string * gameFigure, vector<string*> neighbors);
    void reproduction(string * gameFigure, vector<string*> neighbors);


};

#endif
