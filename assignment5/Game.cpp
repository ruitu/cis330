#include "Game.hpp"

Game::Game(int height, int weight, int steps): _height(height), _weight(weight) , _steps(steps) {
    int base         = 1;
    int fammerWeight = 5  * base;
    int wolfWeight   = 5  * base;
    int sheepWeight  = 20 * base;
    int emptyWeight  = 10 * base;


    vector<string> gameFigures;
    for (int i = 0; i < fammerWeight; i++)
        gameFigures.push_back("F");

    for (int i = 0; i < wolfWeight; i++)
        gameFigures.push_back("W");

    for (int i = 0; i < sheepWeight; i++)
        gameFigures.push_back("S");

    for (int i = 0; i < emptyWeight; i++)
          gameFigures.push_back(".");


    int size = gameFigures.size();

    srand (time(NULL));

    for (int i = 0; i < this->_height; i++) {
        vector<string> tempVec;
        for (int j = 0; j < this->_weight; j++) {
            tempVec.push_back(gameFigures[rand() % size]);
        }
        this->_world.push_back(tempVec);
    }
}

void Game::nextStep() {
    for (int i = 0; i < this->_height; i++) {
        for (int j = 0; j < this->_weight; j++) {
			      reproduction(&(this->_world[i][j]), getNeighbors(i, j));
            if (predation(this->_world[i][j], getNeighbors(i, j))          ||
                wolfKilledByFammer(this->_world[i][j], getNeighbors(i, j)) ||
                starvation(this->_world[i][j], getNeighbors(i, j))         ||
                overpopulation(this->_world[i][j], getNeighbors(i, j))
            ) {
                this->_world[i][j] = ".";
            }
            emigration(&(this->_world[i][j]), getNeighbors(i, j));
        }
    }
}


bool Game::overpopulation(string gameFigure, vector<string*> neighbors) {
    int counter = 0;
    if (gameFigure == "W" || gameFigure == "S") {
        for (int i = 0; i < neighbors.size(); i++) {
            if (*neighbors[i] == gameFigure) {
                counter++;
            }
        }
    }

    return counter >= 3;
}

bool Game::predation(string gameFigure, vector<string*> neighbors){
    bool flag = false;
    if (gameFigure == "S") {
        for (int i = 0; i < neighbors.size(); i++) {
            if (*neighbors[i] == "W") {
                flag = true;
            }
        }
    }

    return flag;
}


// TODO: needs to be redone
void Game::reproduction(string * gameFigure, vector<string*> neighbors) {
    int fc = 0;
    int sc = 0;
    int wc = 0;

    if (*gameFigure == ".") {
        for (int i = 0; i < neighbors.size(); i++) {
            if (*neighbors[i] == "W") {
                wc++;
            } else if (*neighbors[i] == "S") {
                sc++;
            } else if (*neighbors[i] == "F"){
                fc++;
            }
        }
    }

    if (sc == 2) {
        *gameFigure = "S";
    } else if (wc == 2) {
        *gameFigure = "W";
    } else if (fc == 2) {
        *gameFigure = "F";
    }
}


// TODO: implement emigration
void Game::emigration(string * gameFigure, vector<string*> neighbors) {
    std::vector<int> emptyCells;
    srand (time(NULL));
    if (*gameFigure == "F") {
        for (int i = 0; i < neighbors.size(); i++) {
            if (*neighbors[i] == ".") {
                emptyCells.push_back(i);
            }
        }

        int size = emptyCells.size();
        if (size > 0) {
            int randChose = rand() % size;
            *neighbors[emptyCells[randChose]] = "F";
            *gameFigure = ".";
        }
    }
}


bool Game::wolfKilledByFammer(string gameFigure, vector<string*> neighbors) {
    int counter = 0;
    if (gameFigure == "W") {
        for (int i = 0; i < neighbors.size(); i++) {
            if (*neighbors[i] == "F") {
                counter++;
            }
        }
    }

    return counter > 0;
}


bool Game::starvation(string gameFigure, vector<string*> neighbors) {
    int counter = 0;
    int size    = neighbors.size();

    if (gameFigure == "W") {
        for (int i = 0; i < size; i++) {
            if (*neighbors[i] == "W" || *neighbors[i] == ".") {
                counter++;
            }
        }
    }
    return (counter - size) == 0;
}





void Game::printMap() {
    int counter = 0;
    for (int i = 0; i < this->_height; i++) {
        for (int j = 0; j < this->_weight; j++) {
            if (this->_world[i][j] == "F" || this->_world[i][j] == "F+")
                counter++;
            std::cout << this->_world[i][j] << "  ";
        }
        std::cout << std::endl;
    }
}




vector<string * > Game::getNeighbors(int i, int j) {
    vector<string*> neighbors;
    if ( i - 1 >= 0 && j - 1 >=0 )
        neighbors.push_back(&(this->_world[i-1][j-1]));

    if ( i - 1 >= 0)
        neighbors.push_back(&(this->_world[i-1][ j ]));

    if ( i - 1 >= 0 && j + 1 < this->_weight )
        neighbors.push_back(&(this->_world[i-1][j+1]));

    if ( j - 1 >= 0)
        neighbors.push_back(&(this->_world[ i ][j-1]));

    if ( j + 1 < this->_weight )
        neighbors.push_back(&(this->_world[ i ][j+1]));

    if ( i + 1 < this->_height && j - 1 >=0 )
        neighbors.push_back(&(this->_world[i+1][j-1]));

    if ( i + 1 < this->_height)
        neighbors.push_back(&(this->_world[i+1][ j ]));

    if ( i + 1 < this->_height && j + 1 < this->_weight)
        neighbors.push_back(&(this->_world[i+1][j+1]));

    return neighbors;
}
